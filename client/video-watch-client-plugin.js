function register({ registerHook, peertubeHelpers }) {
  initSlides(registerHook, peertubeHelpers).catch(err =>
    console.error("Cannot initialize slides plugin", err)
  )
}

export { register }

function initSlides(registerHook, peertubeHelpers) {
  return peertubeHelpers.getSettings().then(s => {
    registerHook({
      target: "action:video-watch.video.loaded",
      handler: params => {
        window.videojs = params.videojs
        // init contrib-ads
        const slides = require("videojs-slides").default
        params.videojs.registerPlugin("slides", slides)
      }
    })

    registerHook({
      target: "action:video-watch.player.loaded",
      handler: params => {
        fetch(window.location.href, {
          headers: new Headers({
            Accept:
              'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
          })
        })
          .then(r => r.json())
          .then(video => {
            // tokenize content
            const md = require("markdown").markdown
            const tree = md.parse(video.content)
            // console.log(tree)

            const slides = tree
              .find(e => e != "markdown" && e[1].find(f => f == "slides"))[2]
              .split("\n")
              .filter(e => e != "")
              .map(e => e.replace("- ", ""))
              .map(e => e.split(" "))
              .map(e => {
                return {
                  time: (function() {
                    // get seconds from timestamp
                    const s = hmsToSeconds(e[0])
                    return s
                  })(),
                  url: e[1]
                }
              })
            // console.log(slides)

            let resources = tree.find(
              e => e != "markdown" && e[1].find(f => f == "resources")
            )
            if (resources) {
              resources = resources[2]
                .split("\n")
                .filter(e => e != "")
                .map(e => e.replace("- ", ""))
                .map(e => e.split(" "))
                .map(e => {
                  return {
                    name: (function() {
                      let f = [...e]
                      f.pop()
                      return f.join(" ")
                    })(),
                    url: [...e].pop(),
                    cta: "view"
                  }
                })
            } else {
              resources = []
            }
            // console.log(resources)

            // set slides in player
            params.player.slides({
              slides,
              resources,
              author: {
                name: "persed author name",
                image: "/client/assets/images/default-avatar.png"
              },
              links: []
            })
          })
      }
    })
  })
}

function hmsToSeconds(timestamp) {
  return timestamp.split(":").reduce((acc, time) => 60 * acc + +time)
}
