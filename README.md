# Slides

Display images of your slides alongside a video, at specific timestamps. Ideal for conferences and presentations where resources add meaning to the video.

## Read First

This plugin is NOT supported by Framasoft, nor any organisation.

## How to use it

Edit the description of the video to have at least a `slides` block as follows:

```markdown
__slides__
- 00:30 https://your_first_slide.png
- 00:50 https://your_second_slide.jpeg
```

In addition you can add a `resources` block that will point users to documents (generally the slides deck as `.pdf`):

```markdown
__resources__
- Name or description of your file https://your_file.pdf
```

If no `sildes` block is found, the plugin doesn't activate and the video player doesn't get modified.
